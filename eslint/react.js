// @ts-check

import globals from "globals";
import tseslint from "typescript-eslint";

import base from "./index.js";

const astro = tseslint.config(...base, {
  languageOptions: {
    globals: {
      ...globals.browser,
    },
  },
});

export default astro;
