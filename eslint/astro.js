// @ts-check

import eslintPluginAstro from "eslint-plugin-astro";
import tseslint from "typescript-eslint";

import base from "./index.js";

const astro = tseslint.config(
  ...base,
  ...eslintPluginAstro.configs["flat/recommended"],
  ...eslintPluginAstro.configs["flat/jsx-a11y-strict"],
);

export default astro;
