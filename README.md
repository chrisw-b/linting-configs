# Linting Configs

Just a collection of all of my linter setups

**N.B.**: biome does not support extending extends, instead of just using

```json
{
  "$schema": "https://biomejs.dev/schemas/1.7.3/schema.json",
  "extends": ["@chrisw-b/linting-configs/biome/node"]
}
```

you will need to use

```json
{
  "$schema": "https://biomejs.dev/schemas/1.7.3/schema.json",
  "extends": [
    "@chrisw-b/linting-configs/biome/base",
    "@chrisw-b/linting-configs/biome/node"
  ]
}
```
